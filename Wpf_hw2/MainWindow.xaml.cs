﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Wpf_hw2.Models;

namespace Wpf_hw2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //searchTextBox.;


        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            var dateTime = DateTime.Now;
            var weatherMethods = new WeatherMethods();
            var city = weatherMethods.GetWeatherByCity(searchTextBox.Text.ToLower());
            var cityList = city.List;
            day1Date.Text = $"Date: {dateTime.ToString("dd/MM")}";
            day1TempMin.Text = $"Morning: \n{cityList[0].Temp.Morn}";
            day1TempMax.Text = $"Night: \n{cityList[0].Temp.Night}";
            day1TempDay.Text = $"Day: \n{cityList[0].Temp.Day}";
            day1Humidity.Text= $"Humidity: \n{cityList[0].Humidity.ToString()}";
            day1Main.Text = $"Main: \n{cityList[0].Weather[0].Main}";
            day1Pressure.Source= new BitmapImage(new Uri("http://openweathermap.org/img/w/" + cityList[0].Weather[0].Icon + ".png"));

            day2Date.Text = $"Date: {dateTime.AddDays(1).ToString("dd/MM")}";
            day2TempMin.Text = $"Morning: \n{cityList[1].Temp.Morn}";
            day2TempMax.Text = $"Night: \n{cityList[1].Temp.Night}";
            day2TempDay.Text = $"Day: \n{cityList[1].Temp.Day}";
            day2Humidity.Text = $"Humidity: \n{cityList[1].Humidity.ToString()}";
            day2Main.Text = $"Main: \n{cityList[1].Weather[0].Main}";
            day2Pressure.Source = new BitmapImage(new Uri("http://openweathermap.org/img/w/" + cityList[1].Weather[0].Icon + ".png"));

            day3Date.Text = $"Date: {dateTime.AddDays(2).ToString("dd/MM")}";
            day3TempMin.Text = $"Morning: \n{cityList[2].Temp.Morn}";
            day3TempMax.Text = $"Night: \n{cityList[2].Temp.Night}";
            day3TempDay.Text = $"Day: \n{cityList[2].Temp.Day}";
            day3Humidity.Text = $"Humidity: \n{cityList[2].Humidity.ToString()}";
            day3Main.Text = $"Main: \n{cityList[2].Weather[0].Main}";
            day3Pressure.Source = new BitmapImage(new Uri("http://openweathermap.org/img/w/" + cityList[2].Weather[0].Icon + ".png"));

            day4Date.Text = $"Date: {dateTime.AddDays(3).ToString("dd/MM")}";
            day4TempMin.Text = $"Morning: \n{cityList[3].Temp.Morn}";
            day4TempMax.Text = $"Night: \n{cityList[3].Temp.Night}";
            day4TempDay.Text = $"Day: \n{cityList[3].Temp.Day}";
            day4Humidity.Text = $"Humidity: \n{cityList[3].Humidity.ToString()}";
            day4Main.Text = $"Main: \n{cityList[3].Weather[0].Main}";
            day4Pressure.Source = new BitmapImage(new Uri("http://openweathermap.org/img/w/" + cityList[3].Weather[0].Icon + ".png"));

            day5Date.Text = $"Date: {dateTime.AddDays(4).ToString("dd/MM")}";
            day5TempMin.Text = $"Morning: \n{cityList[4].Temp.Morn}";
            day5TempMax.Text = $"Night: \n{cityList[4].Temp.Night}";
            day5TempDay.Text = $"Day: \n{cityList[4].Temp.Day}";
            day5Humidity.Text = $"Humidity: \n{cityList[4].Humidity.ToString()}";
            day5Main.Text = $"Main: \n{cityList[4].Weather[0].Main}";
            day5Pressure.Source = new BitmapImage(new Uri("http://openweathermap.org/img/w/" + cityList[4].Weather[0].Icon + ".png"));

            day6Date.Text = $"Date: {dateTime.AddDays(5).ToString("dd/MM")}";
            day6TempMin.Text = $"Morning: \n{cityList[5].Temp.Morn}";
            day6TempMax.Text = $"Night: \n{cityList[5].Temp.Night}";
            day6TempDay.Text = $"Day: \n{cityList[5].Temp.Day}";
            day6Humidity.Text = $"Humidity: \n{cityList[5].Humidity.ToString()}";
            day6Main.Text = $"Main: \n{cityList[5].Weather[0].Main}";
            day6Pressure.Source = new BitmapImage(new Uri("http://openweathermap.org/img/w/" + cityList[5].Weather[0].Icon + ".png"));

            day7Date.Text = $"Date: {dateTime.AddDays(6).ToString("dd/MM")}";
            day7TempMin.Text = $"Morning: \n{cityList[6].Temp.Morn}";
            day7TempMax.Text = $"Night: \n{cityList[6].Temp.Night}";
            day7TempDay.Text = $"Day: \n{cityList[6].Temp.Day}";
            day7Humidity.Text = $"Humidity: \n{cityList[6].Humidity.ToString()}";
            day7Main.Text = $"Main: \n{cityList[6].Weather[0].Main}";
            day7Pressure.Source = new BitmapImage(new Uri("http://openweathermap.org/img/w/" + cityList[6].Weather[0].Icon + ".png"));
        }
    }
}
