﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Wpf_hw2.Models
{
    public class WeatherMethods
    {
        public WeatherJson GetWeatherByCity(string cityName="astana")
        {
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    var jsonString = webClient.DownloadString($"http://api.openweathermap.org/data/2.5/forecast/daily?q={cityName}&units=metric&cnt=7&appid=833f8169318f975285098017a376c414");

                    var weatherJson = WeatherJson.FromJson(jsonString);

                    return weatherJson;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return null;
                }
            }
        }
    }
}
